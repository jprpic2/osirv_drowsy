# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:32:58 2021

@author: Jakov-PC
"""

import cv2
import os  
from pymediainfo import MediaInfo

def checkRotation(path_video_file):
    rotateCode = None
    rotation = int(float(MediaInfo.parse(path_video_file).video_tracks[0].rotation))
    
    if rotation == 90:
        rotateCode = cv2.ROTATE_90_CLOCKWISE
    elif rotation == 180:
        rotateCode = cv2.ROTATE_180
    elif rotation == 270:
        rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE

    return rotateCode

def correctRotation(frame, rotateCode):  
     return cv2.rotate(frame, rotateCode) 
 
def getLabeledFilePath(drowsiness_level):
    if(drowsiness_level == '0'):
        file_path=r'../images/0'
    elif(drowsiness_level == '5'):
        file_path=r'../images/5'
    else:
        file_path=r'../images/10'
    return file_path

def getDrowsinessLevel(video_name):
    return video_name[:video_name.index('.')]

def extractImages(parent,file_name):
    raw_filename = r'{}/{}'.format(parent,file_name)
    image_path = getLabeledFilePath(getDrowsinessLevel(file_name))
    
    vidcap = cv2.VideoCapture(raw_filename)
    rotateCode = checkRotation(raw_filename)
    
    success,image = vidcap.read()
    count = 0
    while success:
        success,frame = vidcap.read()
        
        # Saving every nth frame
        count+=3
        vidcap.set(1, count)

        if(count>29):
            if rotateCode is not None:
                frame = correctRotation(frame, rotateCode)
            cv2.imwrite(f"{image_path}/sub_{raw_filename[4:6]}_drws_{raw_filename[7:len(raw_filename)-4]}_fr_{int(count/3)}.jpg",frame)
            
        if(count>89):
            count=0
            break
    
def scan_folder(parent):
    # iterate over all the files in directory 'parent'
    for file_name in os.listdir(parent):
        if (file_name.endswith(".mov") or file_name.endswith(".MOV") or file_name.endswith(".mp4")):
            extractImages(parent,file_name)
        else:
            current_path = "".join((parent, "/", file_name))
            if os.path.isdir(current_path):
                # if we're checking a sub-directory, recursively call this method
                scan_folder(current_path)

scan_folder("../")  # Insert parent direcotry's path
